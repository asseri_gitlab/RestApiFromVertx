package dao;

import clients.MysqlClient;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.SqlConnection;
import io.vertx.sqlclient.Tuple;
import pojo.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class ContactDAO {
//comment 6
    public void getRecords(RoutingContext routingContext, Handler<AsyncResult<List>> handler){

        List<JsonObject> list=new ArrayList<>();



        MysqlClient.client.getConnection(ar1 -> {
            SqlConnection conn = ar1.result();

            conn.query("SELECT identifier,name,contact_number FROM contact")
                    .execute(ar2 -> {
                        if (ar2.succeeded()) {
                            RowSet<Row> result = ar2.result();


                            for(Object item: result){

                                JsonObject record=new JsonObject();
                                record.put("id",((Row)item).getInteger("identifier"));
                                record.put("name",((Row)item).getString("name"));
                                record.put("number",((Row)item).getString("contact_number"));
                                list.add(record);
                            }
                            handler.handle(Future.succeededFuture(list));
                        } else {
                            System.out.println("Failure: " + ar2.cause().getMessage());
                        }

                        // Now close the pool
                        conn.close();

                    });



        });






    }

    public void getRecordsByID(RoutingContext routingContext,String id, Handler<AsyncResult<List>> handler){

        List<JsonObject> list=new ArrayList<>();



        MysqlClient.client.getConnection(ar1 -> {
            SqlConnection conn = ar1.result();

            conn.preparedQuery("SELECT identifier,name,contact_number FROM contact where identifier=?")
                    .execute(Tuple.of(id),ar2 -> {
                        if (ar2.succeeded()) {
                            RowSet<Row> result = ar2.result();


                            for(Object item: result){

                                JsonObject record=new JsonObject();
                                record.put("id",((Row)item).getInteger("identifier"));
                                record.put("name",((Row)item).getString("name"));
                                record.put("number",((Row)item).getString("contact_number"));
                                list.add(record);
                            }
                            handler.handle(Future.succeededFuture(list));
                        } else {
                            System.out.println("Failure: " + ar2.cause().getMessage());
                        }

                        // Now close the pool
                        conn.close();

                    });



        });






    }

    public void insertRecord(RoutingContext routingContext, Contact contact, Handler<AsyncResult<Boolean>> handler){





        MysqlClient.client.getConnection(ar1 -> {
            SqlConnection conn = ar1.result();

            conn.preparedQuery("insert into contact values(?,?,?)")
                    .execute(Tuple.of(contact.getId(),contact.getName(), contact.getNumber()), ar2 -> {
                        if (ar2.succeeded()) {

                            handler.handle(Future.succeededFuture(true));
                        } else {
                            System.out.println("Failure: " + ar2.cause().getMessage());
                            handler.handle(Future.failedFuture(ar2.cause().getMessage()));
                        }

                        // Now close the pool
                        conn.close();

                    });



        });






    }

    public void callExternalAPI(RoutingContext routingContext,String id,Handler<AsyncResult<JsonArray>> handler) {


        CircuitBreaker breaker = CircuitBreaker.create("my-circuit-breaker", routingContext.vertx(),
                new CircuitBreakerOptions().setMaxFailures(5).setMaxRetries(5).setTimeout(2000)
        ).retryPolicy(retryCount -> retryCount * 100L);
// ---
// Store the circuit breaker in a field and access it as follows
// ---
        AtomicReference<AtomicInteger> counter = new AtomicReference<>(new AtomicInteger(1));
        breaker.execute(future -> {
            // some code executing with the breaker
            // the code reports failures or success on the given future.
            // if this future is marked as failed, the breaker increased the
            // number of failures
            System.out.println("count: " + counter.get().intValue());
            String service="/api/getContactByID2";
            if(counter.get().intValue()==5){
                service="/api/getContactByID";
            }
            WebClient client = WebClient.create(routingContext.vertx());
            client
                    .post(8081, "localhost", service)
                    .sendJsonObject(new JsonObject()
                                    .put("id", id)
                            , ar -> {


                                if (ar.failed() || ar.result().statusCode() != 200) {
                                    future.fail("HTTP error");
                                    System.out.println(ar.result().body());
                                } else {
                                    // Do something with the response

                                    future.complete(ar.result().bodyAsJsonArray());

                                }
                                counter.get().getAndIncrement();

                            });
        }).setHandler(ar -> {
            // Get the operation result.
            System.out.println("fialure count: " +breaker.failureCount());
            if(ar.succeeded()) {
                handler.handle(Future.succeededFuture((JsonArray) ar.result()));
            }else if(ar.failed()) {
                System.out.println("Failed.... "+ar.cause());
                handler.handle(Future.failedFuture(ar.cause().getMessage()));
            }
        });
    }


}
