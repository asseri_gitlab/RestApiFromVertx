package api;

import dao.ContactDAO;
import pojo.Contact;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ContactAPI {

    private Map<Integer, Contact> contacts = new LinkedHashMap<>();
public ContactAPI(){
   // createSomeData();
}
    /*private void createSomeData() {
        Contact bowmore = new Contact("Bowmore 15 Years Laimrig", "Scotland, Islay");
        products.put(bowmore.getId(), bowmore);
        Contact talisker = new Contact("Talisker 57° North", "Scotland, Island");
        products.put(talisker.getId(), talisker);
    }*/
    public void getAll(RoutingContext routingContext) {


        ContactDAO contactDAO=new ContactDAO();
       Future<List> contactList=Future.future();
        Future<Boolean> temp=Future.future();

        contactDAO.getRecords(routingContext,contactList);

        temp.complete();
        CompositeFuture.join(contactList,temp).setHandler(h->{

            if(h.succeeded()){


                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(contactList.result()));
            }

        });






    }

        public void getByID(RoutingContext routingContext) {

            String id =routingContext.getBodyAsJson().getString("id");
            ContactDAO contactDAO=new ContactDAO();
            contactDAO.getRecordsByID(routingContext,id,result->{
                if(result.succeeded()){


                    routingContext.response()
                            .setStatusCode(200)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(Json.encodePrettily(result.result()));
                }


            });
            //routingContext.getBody().
           //System.out.println(routingContext.getBodyAsString());
           // System.out.println(routingContext.request().getParam("id"));
           // System.out.println(routingContext.getBodyAsString());
            //String id =routingContext.getBodyAsString("id");
            //System.out.println(id);







        }

    public void addOne(RoutingContext routingContext) {
        System.out.println(routingContext.getBodyAsString());
        final Contact contact = Json.decodeValue(routingContext.getBodyAsString(),
                Contact.class);

        ContactDAO contactDAO=new ContactDAO();
        contactDAO.insertRecord(routingContext,contact,hr->{
                if(hr.succeeded()){

                   getAll(routingContext);

                }
        });


    }
    public void deleteOne(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            contacts.remove(idAsInteger);
        }
        routingContext.response().setStatusCode(204).end();
    }

    public void getByIDFromExternalAPI(RoutingContext routingContext) {

        String id =routingContext.getBodyAsJson().getString("id");
        ContactDAO contactDAO=new ContactDAO();
        contactDAO.callExternalAPI(routingContext,id,result->{
            if(result.succeeded()){
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(result.result()));
            }else{
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(result.cause().getMessage()));
            }


        });
        //routingContext.getBody().
        //System.out.println(routingContext.getBodyAsString());
        // System.out.println(routingContext.request().getParam("id"));
        // System.out.println(routingContext.getBodyAsString());
        //String id =routingContext.getBodyAsString("id");
        //System.out.println(id);







    }

    /*private void createSomeData(Handler<AsyncResult<String>> handler) {
        Contact bowmore = new Contact("Bowmore 15 Years Laimrig", "Scotland, Islay");
        products.put(bowmore.getId(), bowmore);
        Contact talisker = new Contact("Talisker 57° North", "Scotland, Island");
        products.put(talisker.getId(), talisker);
        handler.handle(Future.succeededFuture("yes"));
    }*/
}
