import api.ContactAPI;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.redis.RedisClient;


import io.vertx.redis.RedisOptions;
import io.vertx.redis.client.*;
import io.vertx.redis.op.ClientReplyOptions;
import io.vertx.redis.op.SetOptions;
import pojo.Contact;

import java.util.LinkedHashMap;
import java.util.Map;

public class ServerVerticle extends AbstractVerticle {

    private Map<Integer, Contact> products = new LinkedHashMap<>();

    @Override
    public void start(Future<Void> fut) {
        System.out.println("verticle start methods.....");
        Router router = Router.router(vertx);
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Hello from my first Vert.x 3 application</h1>");
        });

        // Serve static resources from the /assets directory
        router.route("/assets/*").handler(StaticHandler.create("assets"));

        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        // Retrieve the port from the configuration,
                        // default to 8080.
                        config().getInteger("http.port", 8080),
                        result -> {
                            if (result.succeeded()) {
                                System.out.println("ServerVerticle created...");
                                fut.complete();
                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );



        ContactAPI contactAPI=new ContactAPI();
        router.route("/api*").handler(BodyHandler.create());
        router.get("/api/contact").handler(contactAPI::getAll);
        router.post("/api/contact").handler(contactAPI::addOne);
        router.post("/api/getContactByID").handler(contactAPI::getByID);
        router.delete("/api/contact/:id").handler(contactAPI::deleteOne);
        router.post("/api/getContactFromExternalAPI").handler(contactAPI::getByIDFromExternalAPI);




   //




            // If a config file is set, read the host and port.
            String host = Vertx.currentContext().config().getString("host");
            if (host == null) {
                host = "127.0.0.1";
            }

            // Create the redis client
            final RedisClient client = RedisClient.create(vertx,
                    new io.vertx.redis.RedisOptions()
                            .setHost(host));

            client .configSet("notify-keyspace-events","AKE",hr1->{
                System.out.println("######2.... "+hr1.result());
            })
                  /* .configSet("notify-keyspace-events","Ex",hr1->{
                System.out.println("######.... "+hr1.result());
            })*/
            /*.publish("__keyevent@0__:expired","hello",hr->{
            if(hr.succeeded() && hr.result()!=null)
                System.out.println("here... :"+hr.result());
            })*/
                    .set("key2", "abdulelah", r -> {
                if (r.succeeded()) {
                    System.out.println("key stored");
                    client.get("key2", s -> {
                        System.out.println("Retrieved value: " + s.result());

                        client.set("key2", "Saud", r2 -> {
                            if (r.succeeded()) {
                                System.out.println("key stored");
                                client.get("key2", s2 -> {
                                    System.out.println("Retrieved value 2: " + s2.result());

                                    //---------------

                                    final RedisClient client2 = RedisClient.create(vertx,
                                            new io.vertx.redis.RedisOptions()
                                                    .setHost("127.0.0.1"));

                                    client2.set("key4","Danah",hhh->{

                                    });


                                });
                            } else {
                                System.out.println("Connection or Operation Failed " + r2.cause());
                            }
                        });
                    });
                } else {
                    System.out.println("Connection or Operation Failed " + r.cause());
                }
            }).setWithOptions("key3", new JsonObject().put("name","saud").encodePrettily(),new SetOptions().setEX(3), r -> {

                    }).clientReply( ClientReplyOptions.ON, hrr->{
                System.out.println("qqqqqqqqqqq.... "+hrr.result());

            });

// example for keyspace

       /* client.configSet("notify-keyspace-events","Ks",hr1->{
            System.out.println("######2.... "+hr1.result());
        })

        .setWithOptions("key2", new JsonObject().put("name","Saud").encodePrettily(),new SetOptions().setEX(5), r -> {
            if (r.succeeded()) {
                System.out.println("key stored");
                client.get("key2", s -> {
                    System.out.println("Retrieved value 2: " + s.result());
                });
            } else {
                System.out.println("Connection or Operation Failed " + r.cause());
            }
        });*/




        /*final RedisClient client2 = RedisClient.create(vertx,
                new io.vertx.redis.RedisOptions()
                        .setHost("127.0.0.1"));

        client2.subscribe("subscribe __keyevent@0__:expired",hr->{
            if(hr.succeeded())
                System.out.println("here4... :"+hr.result());
        }).clientReply( ClientReplyOptions.ON,hrr->{
            System.out.println("qqqqqqqqqqq.... "+hrr.result());

        });
*/

        /*io.vertx.redis.RedisOptions config = new RedisOptions()
                .setHost("127.0.0.1");

        RedisClient redis = RedisClient.create(vertx, config);

         vertx.setPeriodic(1000, t -> {
             client2.subscribe("subscribe __keyevent@0__:expired",hr->{
                 if(hr.succeeded())
                     System.out.println("here4... :"+hr.result());
             });
        });*/



        /*Redis.createClient(vertx, new RedisOptions().setConnectionString("redis://127.0.0.1:6379"))
                .connect(onConnect -> {
                    if (onConnect.succeeded()) {
                        System.out.println("me>>>>");
                        RedisAPI redis = RedisAPI.api(onConnect.result());
                        onConnect.result().send(Request.cmd(Command.APPEND).arg("channel1").arg("Hello World!"),res->{
                            //System.out.println(res.result());
                       });
                        redis.get("key", res -> {
                            if (res.succeeded()) {
                                // so something...
                                System.out.println(res.result());
                            }
                        });
                        RedisConnection client2 = onConnect.result();

                        client2.handler(message -> {
                            System.out.println("message ....: "+message);// do whatever you need to do with your message
                        });
                    }
                });*/

    }


   //
    }


