package pojo;

import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

import java.util.concurrent.atomic.AtomicInteger;

public class Contact {

    private static final AtomicInteger COUNTER = new AtomicInteger();

    private final int id;

    private String name;

    private String number;

    public Contact(String name, String number) {
        this.id = COUNTER.getAndIncrement();
        this.name = name;
        this.number = number;
    }

    public Contact() {
        this.id = COUNTER.getAndIncrement();
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String origin) {
        this.number = origin;
    }


}