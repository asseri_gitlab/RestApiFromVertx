package clients;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.RedisAPI;
import io.vertx.redis.client.RedisConnection;
import io.vertx.redis.client.RedisOptions;
import io.vertx.redis.op.ClientReplyOptions;
import io.vertx.redis.op.SetOptions;

public class RedisListener extends AbstractVerticle  {

    public void start(Future<Void> fut) {




/*
        final RedisClient client2 = RedisClient.create(vertx,
                new io.vertx.redis.RedisOptions()
                        .setHost("127.0.0.1").setPort(6379));

        client2.subscribe("__keyevent@0__:expired",hr->{
            if(hr.succeeded())
                System.out.println("here4... :"+hr.result());
            vertx.eventBus().consumer("__keyevent@0__:expired",hr2->{
                System.out.println("---------: "+hr2.body());
            });

            //fut.complete();
        });
*/




        RedisClient redis = RedisClient.create(vertx, JsonObject.mapFrom(new RedisOptions()));

      /*  redis.subscribe("__keyevent@0__:expired", res -> {
            if (res.succeeded()) {
                // so something...

                vertx.eventBus().<JsonObject>consumer("io.vertx.redis.__keyevent@0__:expired", received -> {
                    // do whatever you need to do with your message
                    JsonObject value = received.body().getJsonObject("value");
                    // the value is a JSON doc with the following properties
                    // channel - The channel to which this message was sent
                    // pattern - Pattern is present if you use psubscribe command and is the pattern that matched this message channel
                    // message - The message payload
                    System.out.println("Expired value......: "+value);
                });
            }
        });
      */ /* redis.get("key", res -> {
            if (res.succeeded()) {
                // so something...
                System.out.println("---------: "+res.result());
            }
        });*/

        redis.subscribe("__keyspace@0__:key3", res -> { // to remember , use keyevent if you want to monitor event base such as set/del/expired
            System.out.println("subscribe... "+res.result());
            vertx.eventBus().<JsonObject>consumer("io.vertx.redis.__keyspace@0__:key3", received -> {
                // do whatever you need to do with your message
                JsonObject value = received.body().getJsonObject("value");
                // the value is a JSON doc with the following properties
                // channel - The channel to which this message was sent
                // pattern - Pattern is present if you use psubscribe command and is the pattern that matched this message channel
                // message - The message payload
                System.out.println("Updated value......: "+value);
            });

        });





        System.out.println("Listener verticle started....");
        fut.complete();
    }

}
