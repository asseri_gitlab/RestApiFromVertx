package clients;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.mysqlclient.MySQLConnectOptions;
import io.vertx.mysqlclient.MySQLConnection;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.PoolOptions;

public class MysqlClient extends AbstractVerticle {

    public static MySQLPool client;

    @Override
    public void start(Future<Void> fut) {
        MySQLConnectOptions connectOptions = new MySQLConnectOptions()
                .setPort(3306)
                .setHost("localhost")
                .setDatabase("world")
                .setUser("asseri")
                .setPassword("admin");

// Pool options
        PoolOptions poolOptions = new PoolOptions()
                .setMaxSize(10);

// Create the client pool
        client = MySQLPool.pool(vertx,connectOptions, poolOptions);
        String connectionUri="mysql://asseri:@world.127.0.0.1:3306";
        MySQLConnection.connect(vertx,connectionUri,res->{

            System.out.println("Database initialized....");
        });


    }






}
